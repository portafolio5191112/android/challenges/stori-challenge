package com.hamon.storichallenge.features.home.domain.repository

import com.hamon.storichallenge.features.home.data.models.MovementsData

interface HomeRepository {
    fun getLastMovements(movements: (MutableList<MovementsData>) -> Unit)
}