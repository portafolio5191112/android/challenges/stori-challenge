package com.hamon.storichallenge.features.home.presentation.bloc

import com.hamon.storichallenge.features.home.domain.usecases.GetMovementsUseCase
import com.ptrbrynt.kotlin_bloc.core.Bloc
import kotlinx.coroutines.DelicateCoroutinesApi
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.launch

@OptIn(DelicateCoroutinesApi::class)
class HomeBloc(private val getMovementsUseCase: GetMovementsUseCase) :
    Bloc<HomeEvent, HomeState>(HomeState.Init) {

    init {
        addObservable()
        add(HomeEvent.GetMovements)
    }

    private fun addObservable() {
        on<HomeEvent> { event ->
            when (event) {
                is HomeEvent.GetMovements -> {
                    emit(HomeState.Loading)
                    getMovementsUseCase.invoke { movements ->
                        GlobalScope.launch {
                            if (movements.isNotEmpty()) {
                                emit(HomeState.HasMovements(movements))
                            } else {
                                emit(HomeState.NotHasMovements)
                            }
                        }
                    }
                }
            }
        }
    }

}