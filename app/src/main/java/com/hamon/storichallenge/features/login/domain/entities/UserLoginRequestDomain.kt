package com.hamon.storichallenge.features.login.domain.entities

data class UserLoginRequestDomain(val userEmail: String, val userPass: String)