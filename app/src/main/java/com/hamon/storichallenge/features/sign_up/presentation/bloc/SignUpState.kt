package com.hamon.storichallenge.features.sign_up.presentation.bloc

import com.hamon.storichallenge.features.sign_up.domain.entities.UserBalanceDomain

sealed class SignUpState {
    data object SignUpInit : SignUpState()
    data object SignUpLoading : SignUpState()

    data class SignUpSuccess(val userBalanceDomain: UserBalanceDomain) : SignUpState()

    data object SignUpError : SignUpState()
}