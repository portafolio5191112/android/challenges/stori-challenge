package com.hamon.storichallenge.features.login.presentation.bloc

import com.hamon.storichallenge.features.login.domain.entities.UserLoginRequestDomain

sealed class LoginEvent {
    data class CheckIfUserExist(val requestLogin: UserLoginRequestDomain) : LoginEvent()
}