package com.hamon.storichallenge.features.home.domain.usecases

import com.hamon.storichallenge.features.home.domain.entities.MovementsDomain
import com.hamon.storichallenge.features.home.domain.entities.getStatus
import com.hamon.storichallenge.features.home.domain.entities.getType
import com.hamon.storichallenge.features.home.domain.repository.HomeRepository

interface GetMovementsUseCase {
    operator fun invoke(movements: (MutableList<MovementsDomain>) -> Unit)
}

class GetMovementsUseCaseImpl(private val repository: HomeRepository) : GetMovementsUseCase {
    override fun invoke(movements: (MutableList<MovementsDomain>) -> Unit) {
        repository.getLastMovements { movementsData ->

            movements.invoke(movementsData.map { movement ->
                MovementsDomain(
                    amount = movement.amount ?: 0f,
                    destiny = movement.destiny ?: "",
                    status = movement.status.getStatus(),
                    type = movement.type.getType(),
                    account = movement.account ?: 0,
                    description = movement.description ?: "",
                    folio = movement.folio ?: 0,
                    date = movement.date ?: ""

                )
            }.toMutableList())
        }
    }

}