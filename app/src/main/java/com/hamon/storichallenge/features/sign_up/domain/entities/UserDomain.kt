package com.hamon.storichallenge.features.sign_up.domain.entities

data class UserDomain(
    val userId: String = "",
    val name: String,
    val lastName: String,
    val email: String,
    val password: String
)