package com.hamon.storichallenge.features.camera.presentation.view

import android.Manifest
import android.content.Context
import android.graphics.Bitmap
import android.util.Log
import android.view.ViewGroup.LayoutParams.MATCH_PARENT
import android.widget.LinearLayout
import androidx.camera.core.ImageCapture
import androidx.camera.core.ImageCaptureException
import androidx.camera.core.ImageProxy
import androidx.camera.view.LifecycleCameraController
import androidx.camera.view.PreviewView
import androidx.compose.foundation.background
import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.padding
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.rounded.AddCircle
import androidx.compose.material3.ExperimentalMaterial3Api
import androidx.compose.material3.FloatingActionButton
import androidx.compose.material3.Icon
import androidx.compose.material3.Scaffold
import androidx.compose.runtime.Composable
import androidx.compose.runtime.LaunchedEffect
import androidx.compose.runtime.remember
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.graphics.toArgb
import androidx.compose.ui.platform.LocalContext
import androidx.compose.ui.platform.LocalLifecycleOwner
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.viewinterop.AndroidView
import androidx.core.content.ContextCompat
import androidx.lifecycle.LifecycleOwner
import com.google.accompanist.permissions.ExperimentalPermissionsApi
import com.google.accompanist.permissions.PermissionState
import com.google.accompanist.permissions.isGranted
import com.google.accompanist.permissions.rememberPermissionState
import com.google.accompanist.permissions.shouldShowRationale
import com.hamon.storichallenge.application.utils.FileDatasource
import com.hamon.storichallenge.features.camera.domain.entities.PhotoIdDomain
import java.io.FileOutputStream
import java.io.IOException
import java.util.concurrent.Executor

@OptIn(ExperimentalPermissionsApi::class, ExperimentalMaterial3Api::class)
@Composable
fun CameraScreen(
    onBack: () -> Unit,
    onPhotoSuccess: (PhotoIdDomain) -> Unit

) {

    val context = LocalContext.current
    val lifecycleOwner: LifecycleOwner = LocalLifecycleOwner.current
    val cameraController: LifecycleCameraController =
        remember { LifecycleCameraController(context) }
    val cameraPermissionState: PermissionState = rememberPermissionState(Manifest.permission.CAMERA)

    // Check permission and launch request
    CheckPermission(cameraPermissionState = cameraPermissionState)

    // Check permission camera and show screen
    if (cameraPermissionState.status.isGranted) {
        // Permission granted
        BodyPermissionSuccess(
            cameraController = cameraController,
            lifecycleOwner = lifecycleOwner,
            context = context,
            onPhotoSuccess = onPhotoSuccess
        )
    } else {
        // Show AlertDialog
        Box(
            modifier = Modifier
                .fillMaxSize()
                .background(Color.DarkGray)
        )
        onBack.invoke()

    }


}

@OptIn(ExperimentalPermissionsApi::class)
@Composable
fun CheckPermission(cameraPermissionState: PermissionState) {
    LaunchedEffect(key1 = Unit) {
        if (!cameraPermissionState.status.isGranted && !cameraPermissionState.status.shouldShowRationale) {
            cameraPermissionState.launchPermissionRequest()
        }
    }
}

@Composable
fun BodyPermissionSuccess(
    context: Context,
    cameraController: LifecycleCameraController,
    lifecycleOwner: LifecycleOwner,
    onPhotoSuccess: (PhotoIdDomain) -> Unit
) {
    Scaffold(modifier = Modifier.fillMaxSize(), floatingActionButton = {
        FloatingActionButton(onClick = {

            val mainExecutor: Executor = ContextCompat.getMainExecutor(context)
            cameraController.takePicture(mainExecutor, object :
                ImageCapture.OnImageCapturedCallback() {

                override fun onCaptureSuccess(image: ImageProxy) {
                    super.onCaptureSuccess(image)
                    val correctedBitMapPath = image.toBitmap().saveInternalReturnPath()
                    onPhotoSuccess.invoke(PhotoIdDomain(path = correctedBitMapPath))
                }

                override fun onError(exception: ImageCaptureException) {
                    super.onError(exception)
                    Log.e("Camera error", "Error in capture image", exception)
                }

            })

        }) {
            Icon(
                Icons.Rounded.AddCircle,
                contentDescription = "Tomar captura"
            )
        }
    }) { innerPaddings ->
        AndroidView(modifier = Modifier
            .fillMaxSize()
            .padding(innerPaddings), factory = { context ->
            PreviewView(context).apply {
                setBackgroundColor(Color.White.toArgb())
                layoutParams = LinearLayout.LayoutParams(MATCH_PARENT, MATCH_PARENT)
                scaleType = PreviewView.ScaleType.FILL_START
                implementationMode = PreviewView.ImplementationMode.COMPATIBLE
            }.also { previewView ->
                previewView.controller = cameraController
                cameraController.bindToLifecycle(lifecycleOwner)
            }
        }, onRelease = {
            cameraController.unbind()
        })
    }
}

fun Bitmap.saveInternalReturnPath(): String {
    val fileTosave = FileDatasource().getFile()
    var fOutStream: FileOutputStream? = null
    try {
        fOutStream = FileOutputStream(fileTosave.path)
        this.compress(Bitmap.CompressFormat.PNG, 100, fOutStream)
    } catch (e: Exception) {

    } finally {
        try {
            fOutStream?.close()
        } catch (exception: IOException) {
            exception.printStackTrace()
        }
    }
    return fileTosave.absolutePath
}

@Preview(showSystemUi = true)
@Composable
fun CameraScreenPreview() {

}