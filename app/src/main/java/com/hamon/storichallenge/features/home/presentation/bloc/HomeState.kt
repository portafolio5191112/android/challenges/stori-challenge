package com.hamon.storichallenge.features.home.presentation.bloc

import com.hamon.storichallenge.features.home.domain.entities.MovementsDomain

sealed class HomeState {
    data object Init : HomeState()
    data object Loading : HomeState()
    data object NotHasMovements : HomeState()
    data class HasMovements(val movements: MutableList<MovementsDomain>) : HomeState()
}