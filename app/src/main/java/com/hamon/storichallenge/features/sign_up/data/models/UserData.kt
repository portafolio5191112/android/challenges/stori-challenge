package com.hamon.storichallenge.features.sign_up.data.models

data class UserData(
    val userId: String? = "",
    val name: String? = "",
    val lastName: String? = "",
    val email: String? = "",
    val password: String? = "",
)