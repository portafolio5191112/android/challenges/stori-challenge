package com.hamon.storichallenge.features.sign_up.domain.usecases

import com.hamon.storichallenge.features.sign_up.domain.entities.UserBalanceDomain
import com.hamon.storichallenge.features.sign_up.domain.entities.UserDomain
import com.hamon.storichallenge.features.sign_up.domain.repository.SignUpRepository

interface CreateUserUseCase {
    operator fun invoke(
        userDomain: UserDomain,
        userCreateSuccess: (Boolean, UserBalanceDomain) -> Unit
    )
}

class CreateUserUseCaseImpl(
    private val repository: SignUpRepository,
) : CreateUserUseCase {
    override fun invoke(
        userDomain: UserDomain,
        userCreateSuccess: (Boolean, UserBalanceDomain) -> Unit
    ) {
        repository.createUser(userDomain) { statusOperation, userBalanceData ->
            userCreateSuccess.invoke(
                statusOperation,
                UserBalanceDomain(userBalanceData?.name ?: "", userBalanceData?.balance ?: 0.0)
            )
        }
    }

}