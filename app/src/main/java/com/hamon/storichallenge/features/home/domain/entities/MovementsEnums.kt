package com.hamon.storichallenge.features.home.domain.entities

enum class MovementStatus(val value: String, val label: String) {
    PENDING(
        value = "PENDING",
        label = "Pendiente"
    ),
    SUCCESS(value = "SUCCESS", label = "Realizado"), CANCEL(value = "CANCEL", label = "Cancelado")
}

fun String?.getStatus(): MovementStatus {
    return when {
        this == MovementStatus.PENDING.value -> MovementStatus.PENDING
        this == MovementStatus.SUCCESS.value -> MovementStatus.SUCCESS
        this == MovementStatus.CANCEL.value -> MovementStatus.CANCEL
        else -> MovementStatus.CANCEL
    }
}

enum class MovementType(val value: String, val label: String) {
    DEPOSIT(value = "DEPOSIT", label = "Deposito"), TRANSFER(
        value = "TRANSFER",
        label = "Transferencia"
    )
}

fun String?.getType(): MovementType {
    return when {
        this == MovementType.DEPOSIT.value -> MovementType.DEPOSIT
        this == MovementType.TRANSFER.value -> MovementType.TRANSFER
        else -> MovementType.TRANSFER
    }
}