package com.hamon.storichallenge.features.sign_up.presentation.bloc

import com.hamon.storichallenge.features.sign_up.domain.usecases.CreateUserUseCase
import com.ptrbrynt.kotlin_bloc.core.Bloc
import kotlinx.coroutines.DelicateCoroutinesApi
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.launch

@OptIn(DelicateCoroutinesApi::class)
data class SignUpBloc(private val createUserUseCase: CreateUserUseCase) :
    Bloc<SignUpEvent, SignUpState>(SignUpState.SignUpInit) {

    init {
        on<SignUpEvent> { event ->
            when (event) {
                is SignUpEvent.CreateUser -> {
                    emit(SignUpState.SignUpLoading)
                    createUserUseCase(event.user) { status, userBalance ->
                        GlobalScope.launch {
                            if (status) {
                                emit(SignUpState.SignUpSuccess(userBalanceDomain = userBalance))
                            } else {
                                emit(SignUpState.SignUpError)
                            }
                        }
                    }
                }
            }
        }
    }

}