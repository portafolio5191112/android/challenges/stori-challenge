package com.hamon.storichallenge.features.login.data.models

data class UserLoginRequestData(val userEmail: String, val userPass: String)