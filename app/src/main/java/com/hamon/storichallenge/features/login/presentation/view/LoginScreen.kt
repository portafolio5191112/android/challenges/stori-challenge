package com.hamon.storichallenge.features.login.presentation.view

import android.annotation.SuppressLint
import androidx.compose.foundation.Image
import androidx.compose.foundation.clickable
import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.Spacer
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.height
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.text.KeyboardActions
import androidx.compose.foundation.text.KeyboardOptions
import androidx.compose.material3.Scaffold
import androidx.compose.material3.SnackbarHost
import androidx.compose.material3.SnackbarHostState
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.runtime.getValue
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.remember
import androidx.compose.runtime.rememberCoroutineScope
import androidx.compose.runtime.setValue
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.focus.FocusDirection
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.platform.LocalFocusManager
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.text.SpanStyle
import androidx.compose.ui.text.buildAnnotatedString
import androidx.compose.ui.text.input.ImeAction
import androidx.compose.ui.text.withStyle
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.dp
import com.hamon.storichallenge.R
import com.hamon.storichallenge.components.buttons.ButtonProps
import com.hamon.storichallenge.components.buttons.StoriButton
import com.hamon.storichallenge.components.dialogs.StoriDialogLoader
import com.hamon.storichallenge.components.text_fields.StoriTextField
import com.hamon.storichallenge.components.text_fields.TextFieldProps
import com.hamon.storichallenge.components.text_view.StoriTextView
import com.hamon.storichallenge.components.text_view.TextViewProps
import com.hamon.storichallenge.components.text_view.TextViewType
import com.hamon.storichallenge.features.login.domain.entities.UserLoginRequestDomain
import com.hamon.storichallenge.features.login.presentation.bloc.LoginBloc
import com.hamon.storichallenge.features.login.presentation.bloc.LoginEvent
import com.hamon.storichallenge.features.login.presentation.bloc.LoginState
import com.hamon.storichallenge.features.sign_up.domain.entities.UserBalanceDomain
import com.ptrbrynt.kotlin_bloc.compose.BlocComposer
import com.ptrbrynt.kotlin_bloc.compose.BlocListener
import kotlinx.coroutines.launch
import org.koin.compose.koinInject

@SuppressLint("CoroutineCreationDuringComposition")
@Composable
fun LoginScreen(
    loginBloc: LoginBloc = koinInject(),
    moveToHomeScreen: (UserBalanceDomain) -> Unit,
    onSignUpUser: () -> Unit
) {

    val scope = rememberCoroutineScope()
    val snackbarHostState = remember {
        SnackbarHostState()
    }
    val focusManager = LocalFocusManager.current


    Scaffold(snackbarHost = {
        SnackbarHost(hostState = snackbarHostState)
    }) { innerPadding ->

        var userEmail: String = ""
        var userPassword: String = ""
        var buttonEnabled by remember { mutableStateOf(false) }
        var showEmailError by remember { mutableStateOf(true) }

        BlocComposer(bloc = loginBloc) { state ->

            // Body
            Column(
                modifier = Modifier
                    .padding(innerPadding)
                    .fillMaxSize(),
                horizontalAlignment = Alignment.CenterHorizontally
            ) {
                Column(
                    horizontalAlignment = Alignment.CenterHorizontally,
                    verticalArrangement = Arrangement.Center
                ) {
                    Spacer(modifier = Modifier.height(60.dp))
                    Image(
                        painter = painterResource(id = R.drawable.ic_app),
                        contentDescription = null
                    )
                    Spacer(modifier = Modifier.height(60.dp))
                    StoriTextView(
                        textViewProps = TextViewProps(
                            text = stringResource(id = R.string.app_name),
                            textViewType = TextViewType.H5
                        )
                    )
                }
                Column(
                    modifier = Modifier.fillMaxSize(),
                    horizontalAlignment = Alignment.CenterHorizontally,
                    verticalArrangement = Arrangement.Center
                ) {
                    StoriTextField(
                        textFieldProps = TextFieldProps(
                            hint = stringResource(id = R.string.user_email),
                            showError = showEmailError,
                            error = stringResource(id = R.string.verify_email),
                            keyboardOptions = KeyboardOptions(imeAction = ImeAction.Next),
                            keyboardActions = KeyboardActions(
                                onNext = { focusManager.moveFocus(FocusDirection.Down) }
                            )
                        ),
                        modifier = Modifier
                            .fillMaxWidth()
                            .padding(horizontal = 16.dp),
                        onTextChange = { text ->
                            userEmail = text
                            buttonEnabled = checkDataNotEmpty(userEmail, userPassword)
                            showEmailError = isEmailValid(userEmail)
                        })
                    Spacer(modifier = Modifier.height(8.dp))
                    StoriTextField(
                        textFieldProps = TextFieldProps(
                            isPassword = true,
                            hint = stringResource(id = R.string.user_pasword),
                            keyboardOptions = KeyboardOptions(imeAction = ImeAction.Done),
                            keyboardActions = KeyboardActions(
                                onNext = { focusManager.moveFocus(FocusDirection.Down) }
                            )
                        ),
                        modifier = Modifier
                            .fillMaxWidth()
                            .padding(horizontal = 16.dp),
                        onTextChange = { text ->
                            userPassword = text
                            buttonEnabled = checkDataNotEmpty(userEmail, userPassword)
                        })
                    Spacer(modifier = Modifier.height(16.dp))
                    StoriButton(
                        props = ButtonProps(
                            title = stringResource(id = R.string.sing_in),
                            isEnabled = buttonEnabled
                        ),
                        onTapButton = {
                            loginBloc.add(
                                LoginEvent.CheckIfUserExist(
                                    UserLoginRequestDomain(
                                        userEmail,
                                        userPassword
                                    )
                                )
                            )
                        })
                    Spacer(modifier = Modifier.height(24.dp))
                    RegisterTextView(tapToRegister = onSignUpUser)
                    Spacer(modifier = Modifier.height(16.dp))
                }
            }

            // Check user exist
            if (state is LoginState.Loading) {
                StoriDialogLoader {}
            }

            // User not exist == show snackbar
            if (state is LoginState.UserNotExist) {
                val userNotExist = stringResource(id = R.string.user_not_exist)
                scope.launch {
                    snackbarHostState.showSnackbar(userNotExist)
                }
            }

        }

        BlocListener(bloc = loginBloc) { state ->
            when (state) {

                is LoginState.UserExist -> {
                    moveToHomeScreen.invoke(state.userBalance)
                }

                else -> {}
            }
        }
    }
}

private fun checkDataNotEmpty(email: String, password: String): Boolean {
    return email.isNotBlank() && password.isNotBlank() && isEmailValid(email)
}

private fun isEmailValid(email: String): Boolean {
    return android.util.Patterns.EMAIL_ADDRESS.matcher(email).matches()
}


@Composable
fun RegisterTextView(tapToRegister: () -> Unit) {
    val annotatedString = buildAnnotatedString {
        append(stringResource(id = R.string.not_has_account))
        withStyle(style = SpanStyle(Color.Blue)) {
            append(stringResource(id = R.string.sign_up_here))
        }
    }
    Text(text = annotatedString, modifier = Modifier.clickable {
        tapToRegister.invoke()
    })
}

@Composable
@Preview(showSystemUi = true)
fun LoginScreenPreview() {
    LoginScreen(moveToHomeScreen = {}, onSignUpUser = {})
}