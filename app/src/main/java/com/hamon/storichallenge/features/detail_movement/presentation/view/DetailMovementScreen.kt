package com.hamon.storichallenge.features.detail_movement.presentation.view

import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.Spacer
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.height
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.rememberScrollState
import androidx.compose.foundation.verticalScroll
import androidx.compose.material3.Scaffold
import androidx.compose.runtime.Composable
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.dp
import com.hamon.storichallenge.R
import com.hamon.storichallenge.components.app_bar.StoriAppBar
import com.hamon.storichallenge.components.text_view.StoriTextView
import com.hamon.storichallenge.components.text_view.TextViewProps
import com.hamon.storichallenge.components.text_view.TextViewType
import com.hamon.storichallenge.features.home.domain.entities.MovementStatus
import com.hamon.storichallenge.features.home.domain.entities.MovementType
import com.hamon.storichallenge.features.home.domain.entities.MovementsDomain

@Composable
fun DetailMovementScreen(movementsDomain: MovementsDomain?, onBack: () -> Unit) {
    Scaffold(topBar = {
        StoriAppBar(title = stringResource(id = R.string.movement_detail), backPress = onBack)
    }) { innerPadding ->
        Column(
            modifier = Modifier
                .padding(innerPadding)
                .fillMaxSize()
                .verticalScroll(rememberScrollState()),
            horizontalAlignment = Alignment.CenterHorizontally,
        ) {
            Spacer(modifier = Modifier.height(40.dp))
            StoriTextView(
                textViewProps = TextViewProps(
                    movementsDomain?.destiny ?: "",
                    textViewType = TextViewType.H5
                )
            )
            Spacer(modifier = Modifier.height(8.dp))
            StoriTextView(
                textViewProps = TextViewProps(
                    if (movementsDomain?.type == MovementType.DEPOSIT) {
                        "+ \$${movementsDomain.amount}"
                    } else {
                        "- \$${movementsDomain?.amount}"
                    },
                    textViewType = TextViewType.H5
                )
            )
            Spacer(modifier = Modifier.height(40.dp))
            Column(
                modifier = Modifier
                    .fillMaxSize()
                    .padding(horizontal = 16.dp)
            ) {
                StoriTextView(
                    textViewProps = TextViewProps(
                        stringResource(id = R.string.account_to_move),
                        textViewType = TextViewType.BODY_1
                    )
                )
                Spacer(modifier = Modifier.height(8.dp))
                StoriTextView(
                    textViewProps = TextViewProps(
                        movementsDomain?.account.toString(),
                        textViewType = TextViewType.BODY_SMALL
                    )
                )
                Spacer(modifier = Modifier.height(16.dp))

                StoriTextView(
                    textViewProps = TextViewProps(
                        stringResource(id = R.string.description),
                        textViewType = TextViewType.BODY_1
                    )
                )
                Spacer(modifier = Modifier.height(8.dp))
                StoriTextView(
                    textViewProps = TextViewProps(
                        movementsDomain?.description ?: "",
                        textViewType = TextViewType.BODY_SMALL
                    )
                )
                Spacer(modifier = Modifier.height(16.dp))

                StoriTextView(
                    textViewProps = TextViewProps(
                        stringResource(id = R.string.detail_operation),
                        textViewType = TextViewType.BODY_1
                    )
                )
                Spacer(modifier = Modifier.height(8.dp))
                StoriTextView(
                    textViewProps = TextViewProps(
                        movementsDomain?.date ?: "",
                        textViewType = TextViewType.BODY_SMALL
                    )
                )
                Spacer(modifier = Modifier.height(16.dp))

                StoriTextView(
                    textViewProps = TextViewProps(
                        stringResource(id = R.string.folio),
                        textViewType = TextViewType.BODY_1
                    )
                )
                Spacer(modifier = Modifier.height(8.dp))
                StoriTextView(
                    textViewProps = TextViewProps(
                        movementsDomain?.folio.toString(),
                        textViewType = TextViewType.BODY_SMALL
                    )
                )
                Spacer(modifier = Modifier.height(16.dp))
            }
        }
    }
}

@Composable
@Preview(showSystemUi = true)
fun DetailMovementScreenPreview() {
    DetailMovementScreen(movementsDomain = MovementsDomain(
        amount = 12f,
        destiny = "Polola",
        status = MovementStatus.CANCEL,
        type = MovementType.DEPOSIT,
        account = 0, description = "Description",
        folio = 123123, date = "asdasd"
    ), onBack = { })
}