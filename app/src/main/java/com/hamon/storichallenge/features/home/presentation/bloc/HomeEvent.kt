package com.hamon.storichallenge.features.home.presentation.bloc

sealed class HomeEvent {
    data object GetMovements: HomeEvent()
}