package com.hamon.storichallenge.features.sign_up.data.datasource

import com.google.firebase.database.DatabaseReference
import com.hamon.storichallenge.application.remote_services.RemoteReferences
import com.hamon.storichallenge.features.sign_up.data.models.UserBalanceData
import com.hamon.storichallenge.features.sign_up.data.models.UserData
import kotlin.math.roundToInt
import kotlin.random.Random

interface SignUpDatasource {
    fun createUser(user: UserData, userCreateSuccess: (Boolean, UserBalanceData?) -> Unit)
}

class SignUpDatasourceImpl(private val database: DatabaseReference) : SignUpDatasource {
    override fun createUser(
        user: UserData,
        userCreateSuccess: (Boolean, UserBalanceData?) -> Unit
    ) {

        // Add
        database.child(RemoteReferences.USER_REFERENCES).child(user.userId ?: "").setValue(user)
            .addOnSuccessListener {

                val userToAdd = UserBalanceData(name = user.name ?: "", balance = randomFloat())

                // Add balance
                database.child(RemoteReferences.BALANCE).child(user.userId ?: "")
                    .setValue(userToAdd)
                    .addOnSuccessListener {
                        userCreateSuccess.invoke(true, userToAdd)
                    }.addOnFailureListener {
                        userCreateSuccess.invoke(false, null)
                    }

            }.addOnFailureListener {
                userCreateSuccess.invoke(false, null)
            }

    }

    private fun randomFloat(): Double {
        return (Random.nextFloat() * 100000).roundToInt() / 100.0
    }

}