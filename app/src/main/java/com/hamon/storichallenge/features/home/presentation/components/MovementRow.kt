package com.hamon.storichallenge.features.home.presentation.components

import androidx.compose.foundation.clickable
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.Spacer
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.height
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.layout.width
import androidx.compose.runtime.Composable
import androidx.compose.ui.Modifier
import androidx.compose.ui.text.style.TextAlign
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.dp
import com.hamon.storichallenge.components.text_view.StoriTextView
import com.hamon.storichallenge.components.text_view.TextViewProps
import com.hamon.storichallenge.components.text_view.TextViewType
import com.hamon.storichallenge.features.home.domain.entities.MovementStatus
import com.hamon.storichallenge.features.home.domain.entities.MovementType
import com.hamon.storichallenge.features.home.domain.entities.MovementsDomain
import java.util.Locale

@Composable
fun MovementRow(movementsDomain: MovementsDomain, onMovementPress: (MovementsDomain) -> Unit) {
    Column(modifier = Modifier.clickable {
        onMovementPress.invoke(movementsDomain)
    }) {
        Spacer(modifier = Modifier.height(8.dp))
        Row(modifier = Modifier.padding(horizontal = 16.dp)) {
            StoriTextView(
                textViewProps = TextViewProps(
                    text = movementsDomain.type.label.capitalize(locale = Locale.getDefault()),
                    textViewType = TextViewType.BODY_1
                )
            )
            Spacer(modifier = Modifier.width(16.dp))
            StoriTextView(
                modifier = Modifier.fillMaxSize(),
                textViewProps = TextViewProps(
                    text = if (movementsDomain.type == MovementType.DEPOSIT) {
                        "+\$${movementsDomain.amount}"
                    } else {
                        "-\$${movementsDomain.amount}"
                    },
                    textAlign = TextAlign.End,
                    textViewType = TextViewType.BODY_1
                )
            )
        }
        Spacer(modifier = Modifier.height(8.dp))
        Row(modifier = Modifier.padding(horizontal = 16.dp)) {
            StoriTextView(
                textViewProps = TextViewProps(
                    text = movementsDomain.destiny,
                    textViewType = TextViewType.BODY_1
                )
            )
            Spacer(modifier = Modifier.width(16.dp))
            StoriTextView(
                modifier = Modifier.fillMaxSize(),
                textViewProps = TextViewProps(
                    text = movementsDomain.status.label.capitalize(locale = Locale.getDefault()),
                    textAlign = TextAlign.End,
                    textViewType = TextViewType.BODY_1
                )
            )
        }
        Spacer(modifier = Modifier.height(8.dp))
    }
}

@Composable
@Preview(showSystemUi = true)
fun MovementRowPreview() {
    MovementRow(
        MovementsDomain(
            amount = 12f,
            destiny = "asdasd",
            status = MovementStatus.CANCEL,
            type = MovementType.DEPOSIT,
            account = 0, description = "",
            folio = 123123, date = "asdasd"
        )
    ) { movementsDomain ->

    }
}