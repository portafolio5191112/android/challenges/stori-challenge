package com.hamon.storichallenge.features.home.data.repositories

import com.hamon.storichallenge.features.home.data.datasource.HomeDatasource
import com.hamon.storichallenge.features.home.data.models.MovementsData
import com.hamon.storichallenge.features.home.domain.repository.HomeRepository

class HomeRepositoryImpl(private val datasource: HomeDatasource) : HomeRepository {

    override fun getLastMovements(movements: (MutableList<MovementsData>) -> Unit) {
        datasource.getLastMovements(movements)
    }
}