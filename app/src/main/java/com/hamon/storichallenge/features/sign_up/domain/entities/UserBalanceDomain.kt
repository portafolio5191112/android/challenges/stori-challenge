package com.hamon.storichallenge.features.sign_up.domain.entities

import android.net.Uri
import com.hamon.storichallenge.application.utils.JsonNavType
import com.hamon.storichallenge.application.utils.fromJson
import com.hamon.storichallenge.application.utils.toJson

data class UserBalanceDomain(val name: String, val balance: Double) {
    override fun toString(): String = Uri.encode(this.toJson())
}

class UserBalanceDomainArgType : JsonNavType<UserBalanceDomain>() {
    override fun fromJsonParse(value: String): UserBalanceDomain =
        value.fromJson<UserBalanceDomain>()

    override fun UserBalanceDomain.getJsonParse(): String = this.toJson()

}