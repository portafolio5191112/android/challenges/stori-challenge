package com.hamon.storichallenge.features.login.data.repositories

import com.hamon.storichallenge.features.login.data.datasource.LoginDataSource
import com.hamon.storichallenge.features.login.data.models.UserLoginRequestData
import com.hamon.storichallenge.features.login.domain.repository.LoginRepository
import com.hamon.storichallenge.features.sign_up.data.models.UserBalanceData

class LoginRepositoryImpl(private val dataSource: LoginDataSource) : LoginRepository {
    override fun checkUserExist(
        request: UserLoginRequestData,
        userExist: (Boolean, UserBalanceData?) -> Unit
    ) {
        dataSource.existUser(request, userExist)
    }

}