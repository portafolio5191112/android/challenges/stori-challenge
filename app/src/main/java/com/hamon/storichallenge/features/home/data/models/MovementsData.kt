package com.hamon.storichallenge.features.home.data.models

data class MovementsData(
    val amount: Float? = 0f,
    val destiny: String? = "",
    val status: String? = "",
    val type: String? = "",
    val account: Int? = 0,
    val date: String? = "",
    val description: String? = "",
    val folio: Int? = 0,
)