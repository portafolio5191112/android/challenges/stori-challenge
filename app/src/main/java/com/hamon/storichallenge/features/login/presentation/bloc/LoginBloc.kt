package com.hamon.storichallenge.features.login.presentation.bloc

import com.hamon.storichallenge.features.login.domain.usecases.GetIfUserExistUseCase
import com.ptrbrynt.kotlin_bloc.core.Bloc
import kotlinx.coroutines.DelicateCoroutinesApi
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.launch

@OptIn(DelicateCoroutinesApi::class)
class LoginBloc(private val getIfUserExistUseCase: GetIfUserExistUseCase) :
    Bloc<LoginEvent, LoginState>(LoginState.Init) {

    init {
        on<LoginEvent> { event ->
            when (event) {
                is LoginEvent.CheckIfUserExist -> {
                    emit(LoginState.Loading)
                    getIfUserExistUseCase(event.requestLogin) { result, userBalance ->
                        GlobalScope.launch {
                            if (result) {
                                emit(LoginState.UserExist(userBalance))
                            } else {
                                emit(LoginState.UserNotExist)
                            }
                        }
                    }
                }
            }
        }
    }

}