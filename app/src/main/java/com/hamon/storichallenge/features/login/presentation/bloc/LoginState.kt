package com.hamon.storichallenge.features.login.presentation.bloc

import com.hamon.storichallenge.features.sign_up.domain.entities.UserBalanceDomain

sealed class LoginState {
    data object Init : LoginState()
    data object Loading : LoginState()
    data class UserExist(val userBalance: UserBalanceDomain) : LoginState()
    data object UserNotExist : LoginState()
}