package com.hamon.storichallenge.features.login.domain.usecases

import com.hamon.storichallenge.features.login.data.models.UserLoginRequestData
import com.hamon.storichallenge.features.login.domain.entities.UserLoginRequestDomain
import com.hamon.storichallenge.features.login.domain.repository.LoginRepository
import com.hamon.storichallenge.features.sign_up.domain.entities.UserBalanceDomain

interface GetIfUserExistUseCase {
    operator fun invoke(
        request: UserLoginRequestDomain,
        userExist: (Boolean, UserBalanceDomain) -> Unit
    )
}

class GetIfUserExistUseCaseImpl(private val repository: LoginRepository) : GetIfUserExistUseCase {
    override fun invoke(
        request: UserLoginRequestDomain,
        userExist: (Boolean, UserBalanceDomain) -> Unit
    ) {
        repository.checkUserExist(
            UserLoginRequestData(request.userEmail, request.userPass)
        ) { exist, userBalance ->
            userExist.invoke(
                exist,
                UserBalanceDomain(
                    name = userBalance?.name ?: "",
                    balance = userBalance?.balance ?: 0.0
                )
            )
        }
    }

}
