package com.hamon.storichallenge.features.login.domain.repository

import com.hamon.storichallenge.features.login.data.models.UserLoginRequestData
import com.hamon.storichallenge.features.sign_up.data.models.UserBalanceData
import com.hamon.storichallenge.features.sign_up.data.models.UserData

interface LoginRepository {
    fun checkUserExist(request: UserLoginRequestData, userExist: (Boolean, UserBalanceData?) -> Unit)
}