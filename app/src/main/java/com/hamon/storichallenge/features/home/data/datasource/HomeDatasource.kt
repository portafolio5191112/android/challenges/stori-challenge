package com.hamon.storichallenge.features.home.data.datasource

import android.util.Log
import com.google.firebase.database.DataSnapshot
import com.google.firebase.database.DatabaseError
import com.google.firebase.database.DatabaseReference
import com.google.firebase.database.ValueEventListener
import com.google.firebase.database.ktx.getValue
import com.hamon.storichallenge.application.remote_services.RemoteReferences
import com.hamon.storichallenge.features.home.data.models.MovementsData

interface HomeDatasource {
    fun getLastMovements(movements: (MutableList<MovementsData>) -> Unit)
}

class HomeDatasourceImpl(private val database: DatabaseReference) : HomeDatasource {
    override fun getLastMovements(movements: (MutableList<MovementsData>) -> Unit) {
        database.child(RemoteReferences.TRANSACTIONS)
            .addListenerForSingleValueEvent(object : ValueEventListener {

                override fun onDataChange(snapshot: DataSnapshot) {
                    if (snapshot.exists()) {
                        val result =
                            snapshot.children.mapNotNull { it.getValue(MovementsData::class.java) }
                                .toMutableList()
                        movements.invoke(result)
                    } else {
                        movements.invoke(mutableListOf<MovementsData>())
                    }
                }

                override fun onCancelled(error: DatabaseError) {
                    movements.invoke(mutableListOf<MovementsData>())
                }

            })
    }


}