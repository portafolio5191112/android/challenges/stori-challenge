package com.hamon.storichallenge.features.sign_up.data.models

data class UserBalanceData(val name: String? = "", val balance: Double? = 0.0)