package com.hamon.storichallenge.features.camera.domain.entities

import android.net.Uri
import com.hamon.storichallenge.application.utils.JsonNavType
import com.hamon.storichallenge.application.utils.fromJson
import com.hamon.storichallenge.application.utils.toJson

data class PhotoIdDomain(val path: String) {
    override fun toString(): String = Uri.encode(this.toJson())
}

class PhotoIdDomainArgType : JsonNavType<PhotoIdDomain>() {
    override fun fromJsonParse(value: String): PhotoIdDomain =
        value.fromJson<PhotoIdDomain>()

    override fun PhotoIdDomain.getJsonParse(): String = this.toJson()

}