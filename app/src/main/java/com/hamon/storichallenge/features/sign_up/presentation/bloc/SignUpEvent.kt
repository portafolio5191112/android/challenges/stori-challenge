package com.hamon.storichallenge.features.sign_up.presentation.bloc

import com.hamon.storichallenge.features.sign_up.domain.entities.UserDomain

sealed class SignUpEvent {
    data class CreateUser(val user: UserDomain) : SignUpEvent()
}