package com.hamon.storichallenge.features.sign_up.data.repositories

import com.hamon.storichallenge.application.utils.md5
import com.hamon.storichallenge.application.utils.sha256
import com.hamon.storichallenge.features.sign_up.data.datasource.SignUpDatasource
import com.hamon.storichallenge.features.sign_up.data.models.UserBalanceData
import com.hamon.storichallenge.features.sign_up.data.models.UserData
import com.hamon.storichallenge.features.sign_up.domain.entities.UserDomain
import com.hamon.storichallenge.features.sign_up.domain.repository.SignUpRepository

class SignUpRepositoryImpl(private val datasource: SignUpDatasource) : SignUpRepository {
    override fun createUser(user: UserDomain, userCreateSuccess: (Boolean, UserBalanceData?) -> Unit) {
        datasource.createUser(
            UserData(
                userId = "${user.email}-${user.password.md5()}".sha256(),
                user.name,
                user.lastName,
                user.email,
                user.password,
            ),
            userCreateSuccess
        )
    }
}