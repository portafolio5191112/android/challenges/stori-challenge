package com.hamon.storichallenge.features.home.presentation.view

import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.PaddingValues
import androidx.compose.foundation.layout.Spacer
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.height
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.lazy.LazyColumn
import androidx.compose.material3.Divider
import androidx.compose.material3.Scaffold
import androidx.compose.runtime.Composable
import androidx.compose.ui.Modifier
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.dp
import com.hamon.storichallenge.components.cards.StoriCard
import com.hamon.storichallenge.components.loader.StoriLoader
import com.hamon.storichallenge.components.text_view.StoriTextView
import com.hamon.storichallenge.components.text_view.TextViewProps
import com.hamon.storichallenge.components.text_view.TextViewType
import com.hamon.storichallenge.features.home.domain.entities.MovementsDomain
import com.hamon.storichallenge.features.home.presentation.bloc.HomeBloc
import com.hamon.storichallenge.features.home.presentation.bloc.HomeState
import com.hamon.storichallenge.features.home.presentation.components.MovementRow
import com.hamon.storichallenge.features.sign_up.domain.entities.UserBalanceDomain
import com.ptrbrynt.kotlin_bloc.compose.BlocComposer
import org.koin.compose.koinInject

@Composable
fun HomeScreen(
    userBalance: UserBalanceDomain? = null,
    bloc: HomeBloc = koinInject(),
    moveToDetailMovement: (MovementsDomain) -> Unit
) {

    BlocComposer(bloc = bloc) { state ->
        Scaffold { innerPadding ->

            Column(
                modifier = Modifier
                    .padding(innerPadding)
                    .fillMaxSize()
            ) {
                Spacer(modifier = Modifier.height(16.dp))
                StoriTextView(
                    modifier = Modifier.padding(horizontal = 16.dp),
                    textViewProps = TextViewProps(
                        text = "Hola, ${userBalance?.name}",
                        textViewType = TextViewType.H5
                    )
                )
                Spacer(modifier = Modifier.height(16.dp))
                StoriCard(balance = userBalance?.balance.toString())
                Spacer(modifier = Modifier.height(16.dp))
                StoriTextView(
                    modifier = Modifier.padding(horizontal = 16.dp),
                    textViewProps = TextViewProps(
                        text = "Movimientos",
                        textViewType = TextViewType.H5
                    )
                )

                // Has Elements
                if (state is HomeState.HasMovements) {
                    Spacer(modifier = Modifier.height(16.dp))
                    LazyColumn(
                        modifier = Modifier.fillMaxSize(),
                        contentPadding = PaddingValues(16.dp)
                    ) {
                        items(state.movements.size) { index ->
                            MovementRow(
                                movementsDomain = state.movements[index],
                                onMovementPress = moveToDetailMovement
                            )
                            Divider()
                        }
                    }
                }
            }

            // Show loader
            if (state is HomeState.Loading) {
                StoriLoader()
            }


        }
    }
}

@Composable
@Preview(showSystemUi = true)
fun HomeScreenPreview() {
    HomeScreen(
        moveToDetailMovement = {})
}