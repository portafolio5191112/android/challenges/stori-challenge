package com.hamon.storichallenge.features.home.domain.entities

import android.net.Uri
import com.hamon.storichallenge.application.utils.JsonNavType
import com.hamon.storichallenge.application.utils.fromJson
import com.hamon.storichallenge.application.utils.toJson

data class MovementsDomain(
    val amount: Float,
    val destiny: String,
    val status: MovementStatus,
    val type: MovementType,
    val account: Int,
    val date: String,
    val description: String,
    val folio: Int,


    ) {
    override fun toString(): String {
        return Uri.encode(this.toJson())
    }

}

class MovementDomainArgType : JsonNavType<MovementsDomain>() {
    override fun fromJsonParse(value: String): MovementsDomain = value.fromJson<MovementsDomain>()

    override fun MovementsDomain.getJsonParse(): String = this.toJson()

}