package com.hamon.storichallenge.features.sign_up.presentation.view

import androidx.compose.foundation.Image
import androidx.compose.foundation.clickable
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.Spacer
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.height
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.rememberScrollState
import androidx.compose.foundation.text.KeyboardOptions
import androidx.compose.foundation.verticalScroll
import androidx.compose.material3.AlertDialog
import androidx.compose.material3.Card
import androidx.compose.material3.Scaffold
import androidx.compose.material3.SnackbarHost
import androidx.compose.material3.SnackbarHostState
import androidx.compose.runtime.Composable
import androidx.compose.runtime.LaunchedEffect
import androidx.compose.runtime.getValue
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.remember
import androidx.compose.runtime.setValue
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.text.input.ImeAction
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.dp
import coil.compose.rememberImagePainter
import com.hamon.storichallenge.R
import com.hamon.storichallenge.application.utils.FileDatasource
import com.hamon.storichallenge.components.app_bar.StoriAppBar
import com.hamon.storichallenge.components.buttons.ButtonProps
import com.hamon.storichallenge.components.buttons.StoriButton
import com.hamon.storichallenge.components.loader.StoriLoader
import com.hamon.storichallenge.components.text_fields.StoriTextField
import com.hamon.storichallenge.components.text_fields.TextFieldProps
import com.hamon.storichallenge.components.text_view.StoriTextView
import com.hamon.storichallenge.components.text_view.TextViewProps
import com.hamon.storichallenge.components.text_view.TextViewType
import com.hamon.storichallenge.features.sign_up.domain.entities.UserBalanceDomain
import com.hamon.storichallenge.features.sign_up.domain.entities.UserDomain
import com.hamon.storichallenge.features.sign_up.presentation.bloc.SignUpBloc
import com.hamon.storichallenge.features.sign_up.presentation.bloc.SignUpEvent
import com.hamon.storichallenge.features.sign_up.presentation.bloc.SignUpState
import com.ptrbrynt.kotlin_bloc.compose.BlocComposer
import java.io.File
import org.koin.compose.koinInject

@Composable
fun SignUpScreen(
    photoPath: String,
    signUpBloc: SignUpBloc = koinInject(),
    onBack: () -> Unit,
    takePhoto: () -> Unit,
    moveToHome: (UserBalanceDomain) -> Unit
) {

    BlocComposer(bloc = signUpBloc) { state ->

        when (state) {
            !is SignUpState.SignUpLoading -> Body(
                photoPath = photoPath,
                onBack = onBack,
                state = state,
                moveToHome = moveToHome,
                signUpBloc = signUpBloc,
                takePhoto = takePhoto
            )

            else -> StoriLoader()
        }

    }

}

@Composable
private fun Body(
    photoPath: String,
    onBack: () -> Unit,
    moveToHome: (UserBalanceDomain) -> Unit,
    takePhoto: () -> Unit,
    state: SignUpState,
    signUpBloc: SignUpBloc
) {

    val snackbarHostState = remember {
        SnackbarHostState()
    }
    val fileDatasoure: FileDatasource = FileDatasource()
    var showError by remember { mutableStateOf(true) }

    var name: String = ""
    var lastName: String = ""
    var email: String = ""
    var password: String = ""
    var passwordRepeat: String = ""

    var buttonEnabled by remember { mutableStateOf(false) }


    Scaffold(
        snackbarHost = { SnackbarHost(snackbarHostState) },
        topBar = {
            StoriAppBar(title = stringResource(id = R.string.user_register), backPress = onBack)
        }) { innerPadding ->
        Column {
            Column(
                modifier = Modifier
                    .padding(innerPadding)
                    .weight(1f)
                    .verticalScroll(rememberScrollState())
                    .fillMaxSize(),
                horizontalAlignment = Alignment.CenterHorizontally
            ) {
                StoriTextField(
                    modifier = Modifier
                        .fillMaxWidth()
                        .padding(horizontal = 16.dp),
                    textFieldProps = TextFieldProps(
                        hint = stringResource(id = R.string.user_name),
                        keyboardOptions = KeyboardOptions(imeAction = ImeAction.Next)
                    )
                ) {
                    name = it
                    buttonEnabled =
                        checkDataNotEmpty(name, lastName, email, password, passwordRepeat)
                }
                Spacer(modifier = Modifier.height(16.dp))
                StoriTextField(
                    modifier = Modifier
                        .fillMaxWidth()
                        .padding(horizontal = 16.dp),
                    textFieldProps = TextFieldProps(
                        hint = stringResource(id = R.string.user_last_name),
                        keyboardOptions = KeyboardOptions(imeAction = ImeAction.Next)
                    ),
                ) {
                    lastName = it
                    buttonEnabled =
                        checkDataNotEmpty(name, lastName, email, password, passwordRepeat)
                }
                Spacer(modifier = Modifier.height(16.dp))
                StoriTextField(
                    modifier = Modifier
                        .fillMaxWidth()
                        .padding(horizontal = 16.dp),
                    textFieldProps = TextFieldProps(
                        hint = stringResource(id = R.string.user_email),
                        keyboardOptions = KeyboardOptions(imeAction = ImeAction.Next)
                    )
                ) {
                    email = it
                    buttonEnabled =
                        checkDataNotEmpty(name, lastName, email, password, passwordRepeat)
                }
                Spacer(modifier = Modifier.height(16.dp))
                StoriTextField(
                    modifier = Modifier
                        .fillMaxWidth()
                        .padding(horizontal = 16.dp),
                    textFieldProps = TextFieldProps(
                        hint = stringResource(id = R.string.user_pasword),
                        isPassword = true,
                        keyboardOptions = KeyboardOptions(imeAction = ImeAction.Next)
                    )
                ) {
                    password = it
                    buttonEnabled =
                        checkDataNotEmpty(name, lastName, email, password, passwordRepeat)
                    showError = checkPasswordAsSame(password, passwordRepeat)
                }
                Spacer(modifier = Modifier.height(16.dp))
                StoriTextField(
                    modifier = Modifier
                        .fillMaxWidth()
                        .padding(horizontal = 16.dp),
                    textFieldProps = TextFieldProps(
                        hint = stringResource(id = R.string.repeat_user_pasword),
                        isPassword = true,
                        showError = showError,
                        error = stringResource(id = R.string.not_password_as_same),
                        keyboardOptions = KeyboardOptions(imeAction = ImeAction.Done)
                    )
                ) {
                    passwordRepeat = it
                    buttonEnabled =
                        checkDataNotEmpty(name, lastName, email, password, passwordRepeat)
                    showError = checkPasswordAsSame(password, passwordRepeat)
                }

                if (photoPath != "none") {
                    Spacer(modifier = Modifier.height(16.dp))
                    Card(modifier = Modifier.padding(horizontal = 16.dp)) {
                        Image(
                            rememberImagePainter(data = File(photoPath)),
                            contentDescription = "ID Photo",
                            modifier = Modifier
                                .fillMaxWidth()
                                .height(200.dp)
                        )
                    }
                }

            }

            Spacer(modifier = Modifier.height(16.dp))
            StoriButton(
                props = ButtonProps(
                    stringResource(id = R.string.take_photo),
                ),
                onTapButton = takePhoto
            )
            Spacer(modifier = Modifier.height(16.dp))
            StoriButton(
                props = ButtonProps(
                    stringResource(id = R.string.sign_up_here_label),
                    isEnabled = buttonEnabled
                ),
                onTapButton = {
                    signUpBloc.add(
                        SignUpEvent.CreateUser(
                            UserDomain(
                                name = name,
                                lastName = lastName,
                                email = email,
                                password = password
                            )
                        )
                    )
                })
            Spacer(modifier = Modifier.height(16.dp))



        }
    }

    if (state is SignUpState.SignUpError) {
        val userNotExist = stringResource(id = R.string.problems_with_create_user)
        LaunchedEffect(key1 = true) {
            snackbarHostState.showSnackbar(userNotExist)
        }
    }

    if (state is SignUpState.SignUpSuccess) {
        AlertDialog(
            title = {
                StoriTextView(
                    textViewProps = TextViewProps(
                        text = "Registro",
                        textViewType = TextViewType.BODY_1
                    )
                )
            },
            text = {
                StoriTextView(
                    textViewProps = TextViewProps(
                        text = "Tu cuenta ha sido creada\n" +
                                "exitosamente”.",
                        textViewType = TextViewType.BODY_1
                    )
                )
            },
            onDismissRequest = {},
            confirmButton = {
                StoriTextView(
                    modifier = Modifier.clickable {
                        moveToHome.invoke(state.userBalanceDomain)
                    },
                    textViewProps = TextViewProps(
                        text = "Aceptar",
                        textViewType = TextViewType.BODY_1
                    )
                )
            },
        )

    }

}

private fun checkDataNotEmpty(
    name: String,
    lastName: String,
    email: String,
    password: String,
    passwordRepat: String
): Boolean {
    return name.isNotBlank() && lastName.isNotBlank() && email.isNotBlank() && password.isNotBlank() && passwordRepat.isNotBlank() && checkPasswordAsSame(
        password,
        passwordRepat
    )
}

private fun checkPasswordAsSame(
    password: String,
    passwordRepat: String
): Boolean {
    return password == passwordRepat
}


@Composable
@Preview(showSystemUi = true)
fun SignUpScreenPreview() {
    SignUpScreen(onBack = { }, moveToHome = {}, takePhoto = {}, photoPath = "")
}