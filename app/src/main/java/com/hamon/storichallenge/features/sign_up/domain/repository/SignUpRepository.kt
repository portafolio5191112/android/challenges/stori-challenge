package com.hamon.storichallenge.features.sign_up.domain.repository

import com.hamon.storichallenge.features.sign_up.data.models.UserBalanceData
import com.hamon.storichallenge.features.sign_up.domain.entities.UserDomain

interface SignUpRepository {
    fun createUser(user: UserDomain, userCreateSuccess: (Boolean, UserBalanceData?) -> Unit)
}