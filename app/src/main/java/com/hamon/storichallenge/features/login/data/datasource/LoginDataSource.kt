package com.hamon.storichallenge.features.login.data.datasource

import com.google.firebase.database.DataSnapshot
import com.google.firebase.database.DatabaseError
import com.google.firebase.database.DatabaseReference
import com.google.firebase.database.ValueEventListener
import com.google.firebase.database.ktx.getValue
import com.hamon.storichallenge.application.remote_services.RemoteReferences
import com.hamon.storichallenge.application.utils.md5
import com.hamon.storichallenge.application.utils.sha256
import com.hamon.storichallenge.features.login.data.models.UserLoginRequestData
import com.hamon.storichallenge.features.sign_up.data.models.UserBalanceData
import com.hamon.storichallenge.features.sign_up.data.models.UserData

interface LoginDataSource {
    fun existUser(
        loginRequestData: UserLoginRequestData,
        userExist: (Boolean, UserBalanceData?) -> Unit
    )
}

class LoginDataSourceImpl(private val database: DatabaseReference) : LoginDataSource {

    override fun existUser(
        loginRequestData: UserLoginRequestData,
        userExist: (Boolean, UserBalanceData?) -> Unit
    ) {

        database.child(RemoteReferences.USER_REFERENCES)
            .child("${loginRequestData.userEmail}-${loginRequestData.userPass.md5()}".sha256())
            .addListenerForSingleValueEvent(object : ValueEventListener {

                override fun onDataChange(snapshot: DataSnapshot) {
                    val userExistValue = snapshot.exists()
                    val user = snapshot.getValue<UserData>()

                    // Get balance data
                    database.child(RemoteReferences.BALANCE)
                        .child("${loginRequestData.userEmail}-${loginRequestData.userPass.md5()}".sha256())
                        .addListenerForSingleValueEvent(object : ValueEventListener {
                            override fun onDataChange(snapshot: DataSnapshot) {

                                if (snapshot.exists()) {

                                    val balanceData = snapshot.getValue(UserBalanceData::class.java)

                                    userExist.invoke(userExistValue, balanceData)
                                } else {
                                    userExist.invoke(false, null)
                                }

                            }

                            override fun onCancelled(error: DatabaseError) {
                                userExist.invoke(false, null)
                            }

                        })

                }

                override fun onCancelled(error: DatabaseError) {
                    userExist.invoke(false, null)
                }

            })
    }
}