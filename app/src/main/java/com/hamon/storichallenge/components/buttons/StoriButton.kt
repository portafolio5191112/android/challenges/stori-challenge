package com.hamon.storichallenge.components.buttons

import androidx.annotation.DrawableRes
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.Spacer
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.height
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.layout.size
import androidx.compose.foundation.layout.width
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.filled.Info
import androidx.compose.material3.Button
import androidx.compose.material3.Icon
import androidx.compose.material3.OutlinedButton
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.vector.ImageVector
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.Dp
import androidx.compose.ui.unit.dp

@Composable
fun StoriButton(props: ButtonProps, onTapButton: () -> Unit) {
    if (props.buttonType == ButtonType.FILLED || props.buttonType == ButtonType.FILLED_ICON) {
        Row(modifier = Modifier.padding(horizontal = props.marginHorizontal)) {
            Button(
                modifier = Modifier.fillMaxWidth().height(45.dp),
                onClick = onTapButton,
                enabled = props.isEnabled
            ) {
                Row(verticalAlignment = Alignment.CenterVertically) {
                    if (props.buttonType == ButtonType.FILLED_ICON) {
                        props.icon?.let {
                            Icon(painter = painterResource(id = it), contentDescription = "")
                        } ?: props.iconVector?.let {
                            Icon(
                                imageVector = it,
                                contentDescription = "",
                                modifier = Modifier.size(props.iconSize)
                            )
                        }
                        Spacer(modifier = Modifier.width(4.dp))
                    }
                    Text(text = props.title)
                }
            }
        }
    } else {
        Row(modifier = Modifier.padding(horizontal = props.marginHorizontal)) {
            OutlinedButton(
                modifier = Modifier.fillMaxWidth().height(45.dp),
                onClick = onTapButton,
                enabled = props.isEnabled
            ) {
                Row(verticalAlignment = Alignment.CenterVertically) {
                    if (props.buttonType == ButtonType.OUTLINED_ICON) {
                        props.icon?.let {
                            Icon(painter = painterResource(id = it), contentDescription = "")
                        } ?: props.iconVector?.let {
                            Icon(
                                imageVector = it,
                                contentDescription = "",
                                modifier = Modifier.size(props.iconSize)
                            )
                        }
                        Spacer(modifier = Modifier.width(4.dp))
                    }
                    Text(text = props.title)
                }
            }
        }
    }
}

@Composable
@Preview(showBackground = true)
fun PreviewPegButtonFilled() {
    StoriButton(ButtonProps(title = "Iniciar sesión con google")) {

    }
}

@Composable
@Preview(showBackground = true)
fun PreviewPegButtonOutlined() {
    StoriButton(
        ButtonProps(
            title = "Iniciar sesión con google",
            buttonType = ButtonType.OUTLINED
        )
    ) {

    }
}

@Composable
@Preview(showBackground = true)
fun PreviewPegButtonFilledDisabled() {
    StoriButton(ButtonProps(title = "Iniciar sesión con google", isEnabled = false)) {

    }
}

@Composable
@Preview(showBackground = true)
fun PreviewPegButtonOutlinedDisabled() {
    StoriButton(
        ButtonProps(
            title = "Iniciar sesión con google",
            buttonType = ButtonType.OUTLINED,
            isEnabled = false
        )
    ) {

    }
}

@Composable
@Preview(showBackground = true)
fun PreviewPegButtonFilledIcon() {
    StoriButton(
        ButtonProps(
            title = "Iniciar sesión con google",
            buttonType = ButtonType.FILLED_ICON,
            iconVector = Icons.Filled.Info
        )
    ) {

    }
}

@Composable
@Preview(showBackground = true)
fun PreviewPegButtonOutlinedIcon() {
    StoriButton(
        ButtonProps(
            title = "Iniciar sesión con google",
            buttonType = ButtonType.OUTLINED_ICON,
            iconVector = Icons.Filled.Info
        )
    ) {

    }
}

data class ButtonProps(
    val title: String,
    val buttonType: ButtonType = ButtonType.FILLED,
    val isEnabled: Boolean = true,
    val isFullWidth: Boolean = true,
    @DrawableRes val icon: Int? = null,
    val iconVector: ImageVector? = null,
    val iconSize: Dp = 24.dp,
    val marginHorizontal: Dp = 16.dp
)

enum class ButtonType {
    FILLED, OUTLINED, FILLED_ICON, OUTLINED_ICON
}