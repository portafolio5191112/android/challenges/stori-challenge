package com.hamon.storichallenge.components.text_view

import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.text.TextStyle
import androidx.compose.ui.text.font.FontFamily
import androidx.compose.ui.text.font.FontStyle
import androidx.compose.ui.text.font.FontWeight
import androidx.compose.ui.text.style.TextAlign
import androidx.compose.ui.text.style.TextDecoration
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.TextUnit
import androidx.compose.ui.unit.sp
import com.hamon.storichallenge.application.theme.Roboto

// TODO: CHECAR COMO HACER PARA QUE ABARQUE TODA EL ROW

@Composable
fun StoriTextView(
    textViewProps: TextViewProps,
    modifier: Modifier = Modifier,
) {
    Text(
        text = textViewProps.text,
        textAlign = textViewProps.textAlign,
        maxLines = textViewProps.maxLines,
        style = TextStyle(
            fontWeight = textViewProps.textViewType.fontWeight,
            fontFamily = textViewProps.textViewType.fontFamily,
            fontStyle = textViewProps.textViewType.fontStyle,
            fontSize = textViewProps.textViewType.fontSize,
            textDecoration = textViewProps.textViewType.textDecoration
        ),
        color = textViewProps.errorColor ?: Color.Unspecified,
        modifier = modifier
    )
}

@Composable
@Preview(showBackground = true)
fun PegTextViewPreview() {
    StoriTextView(TextViewProps(text = "Log In", textViewType = TextViewType.H5))
}

enum class TextViewType(
    val fontSize: TextUnit,
    val fontFamily: FontFamily = Roboto,
    val fontWeight: FontWeight = FontWeight.Normal,
    val fontStyle: FontStyle = FontStyle.Normal,
    val textDecoration: TextDecoration = TextDecoration.None
) {
    H5(fontSize = 24.sp),
    BODY_SMALL(fontSize = 12.sp),
    BODY_1(fontSize = 16.sp),
    BODY_2(fontSize = 14.sp),
    BODY_2_UNDERLINE(fontSize = 14.sp, textDecoration = TextDecoration.Underline)
}

data class TextViewProps(
    val text: String,
    val textViewType: TextViewType,
    val textAlign: TextAlign = TextAlign.Left,
    val errorColor: Color? = null,
    val maxLines: Int = 1
)