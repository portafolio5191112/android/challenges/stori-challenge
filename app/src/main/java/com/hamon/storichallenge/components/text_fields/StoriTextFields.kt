package com.hamon.storichallenge.components.text_fields

import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.Spacer
import androidx.compose.foundation.layout.height
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.foundation.text.KeyboardActions
import androidx.compose.foundation.text.KeyboardOptions
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.filled.Close
import androidx.compose.material.icons.filled.Lock
import androidx.compose.material.icons.outlined.Password
import androidx.compose.material.icons.outlined.Visibility
import androidx.compose.material.icons.outlined.VisibilityOff
import androidx.compose.material3.Icon
import androidx.compose.material3.IconButton
import androidx.compose.material3.MaterialTheme
import androidx.compose.material3.OutlinedTextField
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.runtime.getValue
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.remember
import androidx.compose.runtime.saveable.rememberSaveable
import androidx.compose.runtime.setValue
import androidx.compose.ui.Modifier
import androidx.compose.ui.text.input.PasswordVisualTransformation
import androidx.compose.ui.text.input.VisualTransformation
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.dp
import com.hamon.storichallenge.components.text_view.StoriTextView
import com.hamon.storichallenge.components.text_view.TextViewProps
import com.hamon.storichallenge.components.text_view.TextViewType

@Composable
fun StoriTextField(
    textFieldProps: TextFieldProps,
    modifier: Modifier = Modifier,
    onTextChange: (String) -> Unit
) {

    var text by rememberSaveable { mutableStateOf("") }
    val showPasswordIcon = remember { mutableStateOf(false) }


    Column {
        StoriTextView(
            modifier = Modifier.padding(horizontal = 16.dp),
            textViewProps = TextViewProps(
                text = textFieldProps.fieldName ?: textFieldProps.hint,
                textViewType = TextViewType.BODY_1
            )
        )
        Spacer(modifier = Modifier.height(8.dp))
        OutlinedTextField(
            value = text,
            modifier = modifier,
            enabled = textFieldProps.isEnabled,
            keyboardActions = textFieldProps.keyboardActions,
            keyboardOptions = textFieldProps.keyboardOptions,
            onValueChange = {
                text = it
                onTextChange.invoke(text)
            },
            visualTransformation = if (textFieldProps.isPassword.not()) {
                VisualTransformation.None
            } else {
                if (showPasswordIcon.value) VisualTransformation.None else PasswordVisualTransformation()
            },
            trailingIcon = {
                if (textFieldProps.isPassword) {
                    val (icon, iconColor) = if (showPasswordIcon.value) {
                        Pair(
                            Icons.Outlined.VisibilityOff,
                            MaterialTheme.colorScheme.primary
                        )
                    } else {
                        Pair(Icons.Outlined.Visibility, MaterialTheme.colorScheme.primary)
                    }

                    IconButton(onClick = { showPasswordIcon.value = !showPasswordIcon.value }) {
                        Icon(
                            icon,
                            contentDescription = "Visibility",
                            tint = iconColor
                        )
                    }
                }

            },
            supportingText = {
                if (textFieldProps.showError.not()) {
                    StoriTextView(
                        textViewProps = TextViewProps(
                            text = textFieldProps.error,
                            errorColor = MaterialTheme.colorScheme.error,
                            textViewType = TextViewType.BODY_SMALL
                        )
                    )
                }
            },
            shape = RoundedCornerShape(100.dp),
            placeholder = { Text(textFieldProps.hint) },
            singleLine = textFieldProps.isSingleLine
        )
    }
}

@Composable
@Preview(showBackground = true)
fun PegTextFieldPreview() {
    StoriTextField(textFieldProps = TextFieldProps(hint = "Email")) {

    }
}

data class TextFieldProps(
    val hint: String,
    val fieldName: String? = null,
    val isSingleLine: Boolean = true,
    val isEnabled: Boolean = true,
    val isPassword: Boolean = false,
    val showError: Boolean = false,
    val error: String = "",
    val keyboardOptions: KeyboardOptions = KeyboardOptions.Default,
    val keyboardActions: KeyboardActions = KeyboardActions.Default
)

enum class KeyBoardType {
    TEXT, NUMERIC, PASSWORD
}