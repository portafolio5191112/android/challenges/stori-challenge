package com.hamon.storichallenge.components.cards

import androidx.compose.foundation.BorderStroke
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.material3.MaterialTheme
import androidx.compose.material3.OutlinedCard
import androidx.compose.runtime.Composable
import androidx.compose.ui.Modifier
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.dp
import com.hamon.storichallenge.components.text_view.StoriTextView
import com.hamon.storichallenge.components.text_view.TextViewProps
import com.hamon.storichallenge.components.text_view.TextViewType

@Composable
fun StoriCard(balance: String) {
    OutlinedCard(
        shape = RoundedCornerShape(16.dp),
        modifier = Modifier
            .fillMaxWidth()
            .padding(horizontal = 16.dp),
        border = BorderStroke(1.dp, MaterialTheme.colorScheme.primary)
    ) {
        StoriTextView(
            modifier = Modifier.padding(start = 16.dp, end = 16.dp, top = 16.dp),
            textViewProps = TextViewProps(
                text = "Mi balance",
                textViewType = TextViewType.BODY_1
            )
        )
        StoriTextView(
            modifier = Modifier.padding(16.dp),
            textViewProps = TextViewProps(
                text = "\$${balance}",
                textViewType = TextViewType.H5
            )
        )
    }
}

@Composable
@Preview(showSystemUi = true)
fun StoriCardPreview() {
    StoriCard(balance = "134,45.90")
}