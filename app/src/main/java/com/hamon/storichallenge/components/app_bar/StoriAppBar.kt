package com.hamon.storichallenge.components.app_bar

import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.filled.ArrowBack
import androidx.compose.material3.CenterAlignedTopAppBar
import androidx.compose.material3.ExperimentalMaterial3Api
import androidx.compose.material3.Icon
import androidx.compose.material3.IconButton
import androidx.compose.runtime.Composable
import androidx.compose.ui.tooling.preview.Preview
import com.hamon.storichallenge.components.text_view.StoriTextView
import com.hamon.storichallenge.components.text_view.TextViewProps
import com.hamon.storichallenge.components.text_view.TextViewType

@OptIn(ExperimentalMaterial3Api::class)
@Composable
fun StoriAppBar(title: String, showBack: Boolean = true, backPress: () -> Unit) {
    CenterAlignedTopAppBar(title = {
        StoriTextView(textViewProps = TextViewProps(text = title, textViewType = TextViewType.H5))
    }, navigationIcon = {
        if (showBack) {
            IconButton(onClick = {
                backPress.invoke()
            }) {
                Icon(imageVector = Icons.Filled.ArrowBack, contentDescription = "Nav back")
            }
        }
    })
}

@Composable
@Preview(showBackground = true)
fun StoriAppBarPreview() {
    StoriAppBar("Title") {

    }
}