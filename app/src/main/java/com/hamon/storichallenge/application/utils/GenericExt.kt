package com.hamon.storichallenge.application.utils

import com.google.gson.Gson


inline fun <reified R> String.fromJson(): R {
    return Gson().fromJson(this, R::class.java)
}

fun <T> T.toJson(): String {
    return Gson().toJson(this)
}