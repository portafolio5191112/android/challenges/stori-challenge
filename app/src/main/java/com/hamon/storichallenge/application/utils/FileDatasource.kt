package com.hamon.storichallenge.application.utils

import android.os.Environment
import java.io.File
import java.util.UUID

class FileDatasource {
    private val externalDir = "${Environment.DIRECTORY_DCIM}${File.separator}$RELATIVE_PATH"

    private val currentFileName: String
        get() = "${System.currentTimeMillis()}-${UUID.randomUUID()}"

    private val externalStorage
        get() = Environment.getExternalStoragePublicDirectory(externalDir).apply { mkdirs() }

    val externalFiles
        get() = externalStorage.listFiles()?.sortedByDescending { it.lastModified() }

    val lastPicture get() = externalFiles?.firstOrNull()

    fun getFile(
        extension: String = "jpg",
    ): File = File(externalStorage.path, "$currentFileName.$extension").apply {
        if (parentFile?.exists() == false) parentFile?.mkdirs()
        createNewFile()
    }

    companion object {
        const val RELATIVE_PATH = "oiink_app"
    }

}