package com.hamon.storichallenge.application.di

import com.google.firebase.Firebase
import com.google.firebase.database.database
import com.google.firebase.storage.storage
import com.hamon.storichallenge.features.home.data.datasource.HomeDatasource
import com.hamon.storichallenge.features.home.data.datasource.HomeDatasourceImpl
import com.hamon.storichallenge.features.home.data.repositories.HomeRepositoryImpl
import com.hamon.storichallenge.features.home.domain.repository.HomeRepository
import com.hamon.storichallenge.features.home.domain.usecases.GetMovementsUseCase
import com.hamon.storichallenge.features.home.domain.usecases.GetMovementsUseCaseImpl
import com.hamon.storichallenge.features.home.presentation.bloc.HomeBloc
import com.hamon.storichallenge.features.login.data.datasource.LoginDataSource
import com.hamon.storichallenge.features.login.data.datasource.LoginDataSourceImpl
import com.hamon.storichallenge.features.login.data.repositories.LoginRepositoryImpl
import com.hamon.storichallenge.features.login.domain.repository.LoginRepository
import com.hamon.storichallenge.features.login.domain.usecases.GetIfUserExistUseCase
import com.hamon.storichallenge.features.login.domain.usecases.GetIfUserExistUseCaseImpl
import com.hamon.storichallenge.features.login.presentation.bloc.LoginBloc
import com.hamon.storichallenge.features.sign_up.data.datasource.SignUpDatasource
import com.hamon.storichallenge.features.sign_up.data.datasource.SignUpDatasourceImpl
import com.hamon.storichallenge.features.sign_up.data.repositories.SignUpRepositoryImpl
import com.hamon.storichallenge.features.sign_up.domain.repository.SignUpRepository
import com.hamon.storichallenge.features.sign_up.domain.usecases.CreateUserUseCase
import com.hamon.storichallenge.features.sign_up.domain.usecases.CreateUserUseCaseImpl
import com.hamon.storichallenge.features.sign_up.presentation.bloc.SignUpBloc
import org.koin.core.module.dsl.bind
import org.koin.core.module.dsl.singleOf
import org.koin.dsl.module

val firebaseInstance = module {
    single { Firebase.database.reference }
    single { Firebase.storage }
}

val dataSources = module {
    singleOf(::SignUpDatasourceImpl) { bind<SignUpDatasource>() }
    singleOf(::LoginDataSourceImpl) { bind<LoginDataSource>() }
    single<HomeDatasource> { HomeDatasourceImpl(get()) }
}

val repositories = module {
    singleOf(::SignUpRepositoryImpl) { bind<SignUpRepository>() }
    singleOf(::LoginRepositoryImpl) { bind<LoginRepository>() }
    single<HomeRepository> { HomeRepositoryImpl(get()) }
}

val useCases = module {
    singleOf(::CreateUserUseCaseImpl) { bind<CreateUserUseCase>() }
    singleOf(::GetIfUserExistUseCaseImpl) { bind<GetIfUserExistUseCase>() }
    single<GetMovementsUseCase> { GetMovementsUseCaseImpl(get()) }
}

val viewModels = module {

}

val blocs = module {
    single { SignUpBloc(get()) }
    single { LoginBloc(get()) }
    single { HomeBloc(get()) }
}