package com.hamon.storichallenge.application

import android.app.Application
import com.hamon.storichallenge.application.di.blocs
import com.hamon.storichallenge.application.di.dataSources
import com.hamon.storichallenge.application.di.firebaseInstance
import com.hamon.storichallenge.application.di.repositories
import com.hamon.storichallenge.application.di.useCases
import com.hamon.storichallenge.application.di.viewModels
import org.koin.android.ext.koin.androidContext
import org.koin.android.ext.koin.androidLogger
import org.koin.core.context.startKoin

class OiinkApp : Application() {

    override fun onCreate() {
        super.onCreate()
        startKoin {
            androidLogger()
            androidContext(this@OiinkApp)
            modules(
                listOf(
                    firebaseInstance,
                    dataSources,
                    repositories,
                    useCases,
                    viewModels,
                    blocs
                )
            )
        }
    }

}