package com.hamon.storichallenge.application.core

import androidx.compose.runtime.Composable
import androidx.navigation.compose.NavHost
import androidx.navigation.compose.composable
import androidx.navigation.compose.rememberNavController
import androidx.navigation.navArgument
import com.hamon.storichallenge.application.core.Screen.Home.USER_BALANCE_KEY
import com.hamon.storichallenge.application.core.Screen.MovementDetail.MOVEMENT_DETAIL_KEY
import com.hamon.storichallenge.application.core.Screen.SignUp.PHOTO_PATH_KEY
import com.hamon.storichallenge.application.utils.fromJson
import com.hamon.storichallenge.features.camera.domain.entities.PhotoIdDomain
import com.hamon.storichallenge.features.camera.domain.entities.PhotoIdDomainArgType
import com.hamon.storichallenge.features.camera.presentation.view.CameraScreen
import com.hamon.storichallenge.features.detail_movement.presentation.view.DetailMovementScreen
import com.hamon.storichallenge.features.home.domain.entities.MovementDomainArgType
import com.hamon.storichallenge.features.home.domain.entities.MovementsDomain
import com.hamon.storichallenge.features.home.presentation.view.HomeScreen
import com.hamon.storichallenge.features.login.presentation.view.LoginScreen
import com.hamon.storichallenge.features.sign_up.domain.entities.UserBalanceDomain
import com.hamon.storichallenge.features.sign_up.domain.entities.UserBalanceDomainArgType
import com.hamon.storichallenge.features.sign_up.presentation.view.SignUpScreen

sealed class Screen(val route: String) {
    data object Login : Screen("login_screen")
    data object SignUp : Screen("sign_up_screen") {

        const val PHOTO_PATH_KEY = "photoPath"
        const val ROUTE_WITH_ARG = "sign_up_screen/{photoPath}"
        fun moveToRouteWithData(data: String): String = "$route/${data}"
    }

    data object Home : Screen("home_screen") {

        const val USER_BALANCE_KEY = "userBalance"
        const val ROUTE_WITH_ARG = "home_screen/{userBalance}"
        fun moveToRouteWithData(data: String): String = "$route/${data}"

    }

    data object MovementDetail : Screen("movement_detail_screen") {

        const val MOVEMENT_DETAIL_KEY = "movementDetail"
        const val ROUTE_WITH_ARG = "movement_detail_screen/{movementDetail}"
        fun moveToRouteWithData(data: String): String = "${route}/${data}"
    }

    data object Camera : Screen("camera")
}

@Composable
fun MainNavigationView() {

    // Navigation
    val navController = rememberNavController()

    NavHost(navController = navController, startDestination = Screen.Login.route) {

        // Login
        composable(Screen.Login.route) {
            LoginScreen(moveToHomeScreen = { userBalance ->
                navController.navigate(Screen.Home.moveToRouteWithData(userBalance.toString())) {
                    popUpTo(navController.graph.id) {
                        saveState = true
                        inclusive = true
                    }
                    launchSingleTop = true
                    restoreState = true
                }
            }, onSignUpUser = {
                navController.navigate(Screen.SignUp.moveToRouteWithData(PhotoIdDomain(path = "none").toString()))
            })
        }

        // SignUp
        composable(Screen.SignUp.ROUTE_WITH_ARG, arguments = listOf(navArgument(PHOTO_PATH_KEY) {
            type = PhotoIdDomainArgType()
        })) { navBackStackEntry ->
            val photoPath =
                navBackStackEntry.arguments?.getString(PHOTO_PATH_KEY)?.fromJson<PhotoIdDomain>()
            SignUpScreen(photoPath = photoPath?.path ?: "none", onBack = {
                navController.navigateUp()
            }, moveToHome = { userBalance ->
                navController.navigate(Screen.Home.moveToRouteWithData(userBalance.toString())) {
                    popUpTo(navController.graph.id) {
                        saveState = true
                        inclusive = true
                    }
                    launchSingleTop = true
                    restoreState = true
                }
            }, takePhoto = {
                navController.navigate(Screen.Camera.route)
            })
        }

        // MovementDetails
        composable(
            Screen.MovementDetail.ROUTE_WITH_ARG,
            arguments = listOf(navArgument(MOVEMENT_DETAIL_KEY) {
                type = MovementDomainArgType()
            })
        ) { navBackStackEntry ->
            val movementsDomain = navBackStackEntry.arguments?.getString(MOVEMENT_DETAIL_KEY)
                ?.fromJson<MovementsDomain>()
            DetailMovementScreen(movementsDomain = movementsDomain, onBack = {
                navController.navigateUp()
            })
        }

        // Home
        composable(
            Screen.Home.ROUTE_WITH_ARG,
            arguments = listOf(navArgument(USER_BALANCE_KEY) {
                type = UserBalanceDomainArgType()
            })
        ) { navBackStackEntry ->
            val userBalance = navBackStackEntry.arguments?.getString(USER_BALANCE_KEY)
                ?.fromJson<UserBalanceDomain>()
            HomeScreen(userBalance = userBalance, moveToDetailMovement = {
                navController.navigate(Screen.MovementDetail.moveToRouteWithData(it.toString()))
            })
        }

        // Camera
        composable(Screen.Camera.route) {
            CameraScreen(onBack = {
                navController.navigateUp()
            }, onPhotoSuccess = { imagePath ->
                navController.navigate(Screen.SignUp.moveToRouteWithData(imagePath.toString())) {
                    popUpTo(navController.graph.id) {
                        saveState = true
                        inclusive = true
                    }
                    launchSingleTop = false
                    restoreState = true
                }
            })
        }

    }

}