package com.hamon.storichallenge.application.utils

import java.security.MessageDigest

fun String?.md5(): String {
    if (this.isNullOrBlank()) return ""
    return hashString(this, "MD5")
}

fun String?.sha256(): String {
    if (this.isNullOrBlank()) return ""
    return hashString(this, "SHA-256")
}

private fun hashString(input: String, algorithm: String): String {
    return MessageDigest
        .getInstance(algorithm)
        .digest(input.toByteArray())
        .fold("") { str, it -> str + "%02x".format(it) }
}
