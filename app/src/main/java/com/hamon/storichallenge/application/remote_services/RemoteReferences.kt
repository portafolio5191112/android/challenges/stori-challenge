package com.hamon.storichallenge.application.remote_services

object RemoteReferences {
    const val USER_REFERENCES = "users"
    const val TRANSACTIONS = "transactions"
    const val BALANCE = "balance"
}